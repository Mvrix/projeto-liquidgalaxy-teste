<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Verifica se a criptografia está funcionando de maneira correta
 * add Mariu 26-04
 */

class TestController extends Controller
{
    public function testPasswordHashing()
    {
        $user = new User();
        $user->name = 'John Doe';
        $user->email = 'johndoe@example.com';
        $user->password = Hash::make('password123');
        $user->save();

        return 'User created successfully!';
    }
}
