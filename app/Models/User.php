<?php

namespace App\Models;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Hashing\Argon2IdHasher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * CODIGO PARA DEFINIR PROVIDERS 'USERS' / HASH DAS SENHAS
     * add MARIU
     */
    public function boot()
{
    $this->registerPolicies();

    Auth::provider('users', function ($app, array $config) {
        return new EloquentUserProvider($app['hash'], $config['model']);
    });

    Hash::extend('argon2id', function ($app) {
        return new Argon2IdHasher;
    });
}

}


